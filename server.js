var express = require('express');
var app     = express();
var server  = app.listen(3000, ()=>console.log("Server startovan na portu 3000"));
var io      = require('socket.io').listen(server);

app.use(express.static('static'))
app.set('view engine', 'ejs');
app.get('/', (req, res)=>{
    res.render('index')
})

io.on('connection', (socket)=>{
    
    //server prosledjuje svim korisnicima username i ID ulogovanog korisnika
    socket.on('login', data=>{
        io.emit('novi_korisnik', {username:data.username, id:socket.id})
    })

    //prihvata ID i PORUKU i prosledjuje selektovanom korisniku
    socket.on('send_message', data=>{
        socket.broadcast.to(data.id).emit('new_message', {message:data.message});
    })
})
